﻿using Ecom.CategoryService.BusinessLayer.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Xunit;
using Ecom.CategoryService.BusinessLayer.Interface;
using Ecom.CategoryService.Handlers;
using Ecom.CategoryService.Queries;
using Ecom.CategoryService.Models;
using Shouldly;
using System.Linq;

namespace Category.MicroService.Test.Queries
{
    public class GetCategoriesRequestHandlerTest
    {
        private static Mock<ICategory> _mockRepo;

        public GetCategoriesRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetCategoryService();
        }

        [Fact]
        public async Task GetCategoryListTest()
        {
            var handler = new GetCategoryHandler(_mockRepo.Object);

            var result = await handler.Handle(new GetCategoryQuery(), CancellationToken.None);

            result.ShouldBeOfType<List<EcomCategory>>();

            result.Count().ShouldBe(2);
        }
    } }
  
