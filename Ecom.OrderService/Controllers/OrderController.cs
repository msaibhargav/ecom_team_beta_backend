﻿using Ecom.OrderService.Commands;
using Ecom.OrderService.Models;
using Ecom.OrderService.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Ecom.OrderService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IMediator mediator;

        public OrderController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        [Route("get-order-by-id/{id}")]
        public async Task<IActionResult> GetOrderById(int id)
        {
            var data = await mediator.Send(new GetOrderByIdQuery() { Id = id });
            return Ok(data);

        }

        [HttpPost]
        [Route("place-order")]
        public async Task<IActionResult> PlaceOrder([FromBody] EcomOrders newOrder)
        {
           var data = await mediator.Send(new PlaceOrderCommand() { newOrder = newOrder });
            return Ok(data);

        }
    }
}
