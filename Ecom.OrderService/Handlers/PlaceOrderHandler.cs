﻿using Ecom.OrderService.BusinessLayer.Interface;
using Ecom.OrderService.Commands;
using Ecom.OrderService.Models;
using Ecom.OrderService.Queries;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.OrderService.Handlers
{
    public class PlaceOrderHandler : IRequestHandler<PlaceOrderCommand, EcomOrders>
    {
        IOrder _data;

        public PlaceOrderHandler(IOrder data)
        {
            _data = data;
        }
        public async Task<EcomOrders> Handle(PlaceOrderCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.PlaceOrder(request.newOrder));
        }
    }
}
