﻿using Ecom.OrderService.Models;
using MediatR;

namespace Ecom.OrderService.Queries
{
    public class GetOrderByIdQuery:IRequest<EcomOrders>
    {
        public int Id { get; set; }
    }
}
