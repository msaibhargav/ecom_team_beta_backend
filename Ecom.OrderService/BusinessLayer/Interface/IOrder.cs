﻿using Ecom.OrderService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ecom.OrderService.BusinessLayer.Interface
{
    public interface IOrder
    {
        Task<EcomOrders> GetOrderById(int id);
        EcomOrders PlaceOrder(EcomOrders newOrder);

    }
}
