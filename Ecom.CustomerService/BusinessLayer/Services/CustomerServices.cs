﻿using Ecom.CategoryService.Models;
using Ecom.CustomerService.BusinessLayer.Interface;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecom.CustomerService.BusinessLayer.Services
{
    public class CustomerServices : ICustomer
    {

        private EcomContext _context;

        public CustomerServices(EcomContext ecomContext)
        {
            _context = ecomContext;
        }
        public EcomCustomers AddNewCustomer(EcomCustomers newCustomer)
        {
            var x = _context.EcomCustomers.Add(newCustomer);
            _context.SaveChanges();
            return newCustomer;
        }

        public async Task<EcomCustomers> GetCustomerById(int id)
        {
            return await Task.FromResult(_context.EcomCustomers.SingleOrDefault(x => x.CustomerId == id));


        }
    }
}
