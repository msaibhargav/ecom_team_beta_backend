﻿using Ecom.CategoryService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ecom.CustomerService.BusinessLayer.Interface
{
    public interface ICustomer
    {
        EcomCustomers AddNewCustomer(EcomCustomers newCustomer);
        Task<EcomCustomers> GetCustomerById(int id);
    }
}
