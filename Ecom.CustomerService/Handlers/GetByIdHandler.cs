﻿using Ecom.CategoryService.Models;
using Ecom.CustomerService.BusinessLayer.Interface;
using Ecom.CustomerService.Queries;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.CustomerService.Handlers
{
    public class GetByIdHandler : IRequestHandler<GetByIdQuery, EcomCustomers>
    {
        ICustomer _data;

        public GetByIdHandler(ICustomer data)
        {
            _data = data;
        }
        public async Task<EcomCustomers> Handle(GetByIdQuery request, CancellationToken cancellationToken)
        {
            return await await Task.FromResult(_data.GetCustomerById(request.Id));
        }
    }
}
