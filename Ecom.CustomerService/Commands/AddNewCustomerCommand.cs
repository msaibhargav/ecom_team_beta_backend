﻿using Ecom.CategoryService.Models;
using MediatR;
using System.Collections.Generic;

namespace Ecom.CustomerService.Commands
{
    public class AddNewCustomerCommand :IRequest<EcomCustomers>
    {
        public EcomCustomers Customer { get; set; }
    }
}
