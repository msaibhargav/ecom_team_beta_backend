﻿using Ecom.CategoryService.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ecom.CategoryService.BusinessLayer.Interface
{
    public interface ICategory
    {
        IEnumerable<EcomCategory> GetAllCategory();
        IEnumerable<EcomCategory> AddNewCategory(EcomCategory newCategory);
        EcomCategory EditById(int id,string editCategoryName);

    }
}
