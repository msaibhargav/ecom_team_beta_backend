﻿using Ecom.CategoryService.Models;
using MediatR;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace Ecom.CategoryService.Commands
{
    public class EditCategoryCommand: IRequest<EcomCategory>
    {

        public int id { get; set; }
        public string newCategoryName { get; set; }

    }
}
