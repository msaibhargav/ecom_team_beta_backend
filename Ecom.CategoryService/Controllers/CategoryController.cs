﻿using Ecom.CategoryService.Commands;
using Ecom.CategoryService.Handlers;
using Ecom.CategoryService.Models;
using Ecom.CategoryService.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ecom.CategoryService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {

        private readonly IMediator mediator;

        public CategoryController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        [HttpGet]
        [Route("get-all-category")]
        public async Task<IActionResult> GetCategory()
        {
            var data = await mediator.Send(new GetCategoryQuery());
            return Ok(data);

        }

        [HttpPost]
        [Route("add-new-category")]
        public async Task<IActionResult> AddCategory([FromBody] EcomCategory newCategory)
        {
            var data = await mediator.Send(new AddNewCategoryCommand() { EcomCategory = newCategory });
            return Ok(data);

        }

        [HttpPut]
        [Route("edit-category")]
        public async Task<IActionResult> EditCategory(int id, string newCategoryName)
        {
            var data = await mediator.Send(new EditCategoryCommand() { id = id, newCategoryName = newCategoryName });
            return Ok(data);
        }
    }
}
