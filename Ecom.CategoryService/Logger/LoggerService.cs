﻿using Microsoft.Extensions.Logging;
using NLog;

namespace Ecom.CategoryService.Logger
{
    public class LoggerService : ILogger
    {
        private static ILogger logger = (ILogger)LogManager.GetCurrentClassLogger();
        public LoggerService()
        {
        }

        public void Error(string message)
        {
            logger.LogDebug(message);
        }

        public void LogDebug(string message)
        {
            throw new System.NotImplementedException();
        }

        public void LogError(string message)
        {
            throw new System.NotImplementedException();
        }

        public void LogInfo(string message)
        {
            throw new System.NotImplementedException();
        }

        public void LogTrace(string message)
        {
            throw new System.NotImplementedException();
        }

        public void LogWarn(string message)
        {
            throw new System.NotImplementedException();
        }
    }

}

