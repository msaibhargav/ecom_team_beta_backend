﻿using Ecom.ProductService.BusinessLayer.Interface;
using Ecom.ProductService.Commands;
using Ecom.ProductService.Models;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Ecom.ProductService.Handlers
{
    public class DeleteProductHandler : IRequestHandler<DeleteProductCommand, IEnumerable<EcomProducts>>

    {
        IProducts _data;

        public DeleteProductHandler(IProducts data)
        {
            _data = data;
        }
        public async Task<IEnumerable<EcomProducts>> Handle(DeleteProductCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.DeleteProduct(request.EcomProducts));
        }
    }
}
