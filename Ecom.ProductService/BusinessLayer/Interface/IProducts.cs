﻿using Ecom.ProductService.Models;
using System.Collections.Generic;

namespace Ecom.ProductService.BusinessLayer.Interface
{
    public interface IProducts
    {
        IEnumerable<EcomProducts> GetAllProducts();
        IEnumerable<EcomProducts> AddProduct(EcomProducts newProduct);
        EcomProducts EditProduct(EcomProducts newProduct);

        IEnumerable<EcomProducts> GetProductsByCategoryId(int id);
        EcomProducts GetProductbyId(int id);
        IEnumerable<EcomProducts> DeleteProduct(EcomProducts deleteProduct);
    }
}
