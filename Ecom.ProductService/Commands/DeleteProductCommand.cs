﻿using Ecom.ProductService.Models;
using MediatR;
using System.Collections.Generic;

namespace Ecom.ProductService.Commands
{
    public class DeleteProductCommand: IRequest<IEnumerable<EcomProducts>>
    {
        public EcomProducts EcomProducts { get; set; }
    }
}
