﻿using Ecom.ProductService.Models;
using MediatR;
using System.Collections.Generic;

namespace Ecom.ProductService.Queries
{
    public class GetProductsQuery: IRequest<IEnumerable<EcomProducts>>
    {
        /*
            No input 
        */
    }
}
