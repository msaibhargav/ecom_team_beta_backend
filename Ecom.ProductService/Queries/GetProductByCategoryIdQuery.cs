﻿using Ecom.ProductService.Models;
using MediatR;
using System.Collections.Generic;

namespace Ecom.ProductService.Queries
{
    public class GetProductByCategoryIdQuery:IRequest<IEnumerable<EcomProducts>>
    {
        public int Id { get; set; }
    }
}
