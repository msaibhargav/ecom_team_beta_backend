﻿using Ecom.LoginService.BusinessLayer.Interface;
using Ecom.LoginService.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Ecom.LoginService.BusinessLayer.Services
{
    public class LoginServices : ILogin
    {
        private EcomContext _context;
        private IConfiguration _configuration;

        public LoginServices(EcomContext ecomContext, IConfiguration config)
        {
            _configuration = config;
            _context = ecomContext;
        }

        private string GenerateToken(EcomLogin user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier,user.UserEmail),
                new Claim(ClaimTypes.Role,user.LoginRole)
            };
            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
                _configuration["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddMinutes(15),
                signingCredentials: credentials);



            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public EcomLogin AddLogin(EcomLogin login)
        {
            var generatedToken = GenerateToken(login);
            login.Token = generatedToken;
            DateTime aDate = DateTime.Now;
            login.DateTimeStamp = Convert.ToDateTime(aDate.ToString("dddd, dd MMMM yyyy HH:mm:ss"));
            _context.Add(login);
            _context.SaveChanges();
            return _context.EcomLogin.SingleOrDefault(x => x.Token == generatedToken);
        }

        public async Task<EcomLogin> GetLoginDetailsById(int id)
        {
            return await Task.FromResult(_context.EcomLogin.SingleOrDefault(x => x.LoginId == id));
        }

        async Task<EcomLogin> ILogin.GetLoginDetailsByCustomerEmail(string customerEmail)
        {
            return await Task.FromResult(_context.EcomLogin.SingleOrDefault(x => x.UserEmail == customerEmail));
        }
    }
}
