﻿using Ecom.LoginService.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ecom.LoginService.BusinessLayer.Interface
{
    public interface ILogin
    {
        EcomLogin AddLogin(EcomLogin login);
        Task<EcomLogin> GetLoginDetailsById(int id);
        Task<EcomLogin> GetLoginDetailsByCustomerEmail(string customerEmail);

    }
}
