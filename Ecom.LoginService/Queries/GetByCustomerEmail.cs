﻿using Ecom.LoginService.Models;
using MediatR;

namespace Ecom.LoginService.Queries
{
    public class GetByCustomerEmail :IRequest<EcomLogin>
    {
        public string Email { get; set; }
    }
}
