﻿using Ecom.LoginService.Models;
using MediatR;

namespace Ecom.LoginService.Commands
{
    public class NewLoginCommand:IRequest<EcomLogin>
    {
        public EcomLogin newLogin { get; set; }
    }
}
