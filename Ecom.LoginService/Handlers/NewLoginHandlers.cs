﻿using Ecom.LoginService.BusinessLayer.Interface;
using Ecom.LoginService.Commands;
using Ecom.LoginService.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Ecom.LoginService.Handlers
{
    public class NewLoginHandlers : IRequestHandler<NewLoginCommand,EcomLogin>
    {
        ILogin _data;

        public NewLoginHandlers(ILogin data)
        {
            _data = data;
        }

        public async Task<EcomLogin> Handle(NewLoginCommand request, CancellationToken cancellationToken)
        {
            return await Task.FromResult(_data.AddLogin(request.newLogin));
        }
    }
}
