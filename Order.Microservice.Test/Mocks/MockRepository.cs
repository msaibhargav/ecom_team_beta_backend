﻿using Ecom.OrderService.BusinessLayer.Interface;
using Ecom.OrderService.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Customer.WebService.Test.Mocks
{
    public static class MockRepository
    {
        public static Mock<IOrder> GetOrderService()
        {
            var orderlist = new List<EcomOrders>
            {
                new EcomOrders
                {
                    OrderId =101,
                    OrderPrice=9000,
                    OrderQuantity=1,
                    CustomerId=1,
                    ProductId=1,
                    ShipmentAddress="Deoghar"
                }
                
            };

            var mockrepo = new Mock<IOrder>();

            mockrepo.Setup(r => r.GetOrderById(101)).ReturnsAsync(orderlist.SingleOrDefault(x => x.OrderId == 101));

            return mockrepo;
        }
    }
}
