﻿using Ecom.OrderService.BusinessLayer.Interface;
using Ecom.OrderService.Commands;
using Ecom.OrderService.Handlers;
using Ecom.OrderService.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Order.Microservice.Test.Commands
{
    public class AddOrderHandlerTest
    {
        private readonly Mock<IOrder> _mockRepo;
        private readonly EcomOrders _order;

        public AddOrderHandlerTest()
        {
            _order = new EcomOrders
            {
                OrderId = 104,
                OrderPrice = 9000,
                OrderQuantity = 1,
                CustomerId = 1,
                ProductId = 1,
                ShipmentAddress = "Deoghar"
            };
        }

        [Fact]
        public async Task AddOrderTest()
        {
            var handler = new PlaceOrderHandler(_mockRepo.Object);

            var result = handler.Handle(new PlaceOrderCommand(), CancellationToken.None);
        }
    }
}

