﻿using Ecom.OrderService.BusinessLayer.Interface;
using Ecom.OrderService.Handlers;
using Ecom.OrderService.Models;
using Ecom.OrderService.Queries;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Customer.WebService.Test.Queries
{
    public class GetOrderByIdRequestHandlerTest
    {
        private readonly Mock<IOrder> _mockRepo;

        public GetOrderByIdRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetOrderService();
        }

        [Fact]
        public async Task GetCustomerByIdTest()
        {
            var handler = new GetOrderByIdHandler(_mockRepo.Object);

            var result = handler.Handle(new GetOrderByIdQuery(), CancellationToken.None);

            await result.ShouldBeOfType<Task<EcomOrders>>();
        }
    }
}
