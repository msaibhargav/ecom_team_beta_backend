﻿using Ecom.PaymentService.Commands;
using Ecom.PaymentService.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Ecom.PaymentService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController: ControllerBase
    {
        private readonly IMediator mediator;
        public PaymentController(IMediator mediator)
        {
            this.mediator = mediator;
        }


        [HttpPost]
        [Route("make-payment")]
        public async Task<IActionResult> MakeNewPayment([FromBody] EcomPayment newProduct)
        {
            var data = await mediator.Send(new MakePaymentCommand() { Payment = newProduct });
            return Ok(data);

        }
    }
}
