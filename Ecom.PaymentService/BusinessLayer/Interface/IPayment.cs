﻿using Ecom.PaymentService.Models;
using System.Collections.Generic;

namespace Ecom.PaymentService.BusinessLayer.Interface
{
    public interface IPayment
    {
        EcomPayment MakePayment(EcomPayment newPayment);


    }
}
