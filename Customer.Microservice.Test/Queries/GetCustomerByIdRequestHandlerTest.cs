﻿using Ecom.CategoryService.Models;
using Ecom.CustomerService.BusinessLayer.Interface;
using Ecom.CustomerService.BusinessLayer.Services;
using Ecom.CustomerService.Handlers;
using Ecom.CustomerService.Queries;
using Moq;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace Customer.WebService.Test.Queries
{
    public class GetCustomerByIdRequestHandlerTest
    {
        private readonly Mock<ICustomer> _mockRepo;

        public GetCustomerByIdRequestHandlerTest()
        {
            _mockRepo = Mocks.MockRepository.GetCustomerService();
        }

        [Fact]
        public async Task GetCustomerByIdTest()
        {
            var handler = new GetByIdHandler(_mockRepo.Object);

            var result = handler.Handle(new GetByIdQuery(), CancellationToken.None);

            await result.ShouldBeOfType<Task<EcomCustomers>>();
        }
    }
}
